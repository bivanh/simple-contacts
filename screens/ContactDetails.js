import React from 'react'
import { Alert, Text, View} from 'react-native'
import { Avatar, Button, Icon, Input } from 'react-native-elements'
import RestRequester from '../api/RestRequester';

class ContactDetails extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {}

    return {
      headerRight: params.headerRight,
      headerStyle: params.headerStyle,
      headerTintColor: params.headerTintColor
    }
  }
  state = {
    id : '',
    firstName: '',
    lastName: '',
    age: 0,
    photo: 'N/A'
  }
  componentDidMount () {
    this._setNavigationParams()
    const { navigation } = this.props
    const itemId = navigation.getParam('contactId', '')
    if (itemId) {
      this.setState({ id: itemId })
      this._getContact(itemId)
    }
  }
  
  _getContact = (itemId) => {
    console.log('getting contact', itemId)
    RestRequester.getContactById(itemId)
      .then(
        response => {
          console.log('response from get by id api', response)
          const contact = response.data.data
          this.setState({
            firstName: contact.firstName,
            lastName: contact.lastName,
            age: contact.age,
            photo: contact.photo
          })
        }
      )
      .catch(
        error => console.log(error)
      )
  }

  _getInitials = (string) => {
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  }

  _navigateToTop = () => {
    this.props.navigation.popToTop()
  }

  _onSave = () => {
    console.log('saving')
    if (this.state.id) {
      RestRequester.updateContact(this.state.id, this.state.firstName, this.state.lastName, this.state.age, this.state.photo)
        .then(
          response => {
            if(response.status === 201) {
              Alert.alert(
                'Success', 
                response.data.message,
                [
                  {text: 'OK', onPress: () => this._navigateToTop()},
                ],
                {cancelable: false},)
            } else {
              Alert.alert(
                'Error', 
                response.data.message)
            }
            console.log('response from update api', response)
          }
        )
        .catch(
          error => console.log(error)
        )
    } else {
      RestRequester.saveContact(this.state.firstName, this.state.lastName, this.state.age, this.state.photo)
        .then(
          response => {
            if(response.status === 201) {
              Alert.alert(
                'Success', 
                response.data.message,
                [
                  {text: 'OK', onPress: () => this._navigateToTop()},
                ],
                {cancelable: false},)
            } else {
              Alert.alert(
                'Error', 
                response.data.message)
            }
          }
        )
        .catch(
          error => console.log(error)
        )
    }
  }
  
  _setNavigationParams = () => {
    let headerRight =
    <View>
      <Button 
        icon={
          <Icon
            name="save"
            color="white"
          />
        }
        type='clear' onPress={ () => this._onSave()}/>
    </View>
    let headerStyle = {
      backgroundColor: '#2d6882'
    }
    let headerTintColor = 'white'

    this.props.navigation.setParams({
      headerRight,
      headerStyle,
      headerTintColor
    })
  }
  
  render() {
    const { navigation } = this.props
    const itemId = navigation.getParam('contactId', 'NO-ID')
    const initials = this._getInitials([this.state.firstName, this.state.lastName].join(' '))
    return(
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Avatar
          rounded
          size='large'
          source={{
            uri:
              this.state.photo,
          }}
          title={initials}
        />
        <Input
          placeholder='Photo Url'
          value={ this.state.photo }
          onChangeText={(value) => this.setState({ photo: value }) }
        />
        
        <Input
          placeholder='First Name'
          value={ this.state.firstName }
          onChangeText={(value) => this.setState({ firstName: value }) }
        />
        <Input
          placeholder='Last Name'
          value={ this.state.lastName }
          onChangeText={(value) => this.setState({ lastName: value }) }
        />
        <Input
          placeholder='Age'
          value={ this.state.age + ''}
          onChangeText={(value) => this.setState({ age: value }) }
        />
      </View>
    )
  }
}

export default ContactDetails
