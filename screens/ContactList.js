import React from 'react'
import {
  Alert,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View
} from 'react-native'
import { Button, Icon} from 'react-native-elements'
import RestRequester from '../api/RestRequester'
import ContactRow from '../components/ContactRow'

let didFocusSubscription = null

class ContactList extends React.Component {
  state = {
    loading: false,
    contacts: []
  }
  
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {}

    return {
      title: params.title,
      headerRight: params.headerRight,
      headerStyle: params.headerStyle,
      headerTintColor: params.headerTintColor
    }
  }

  componentDidMount () {
    this._setNavigationParams()
    if (!didFocusSubscription) {
      didFocusSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {
          this._getContacts()
        }
      )
    }
    this._getContacts()
  }

  componentWillUnmount () {
    if (didFocusSubscription) {
      didFocusSubscription.remove()
      didFocusSubscription = null
    }
  }

  _getContacts = async () => {
    this.setState({loading: true})
    RestRequester.getContacts()
    .then(async response => {
      console.log('response from api', response)
      this.setState({
        loading:false,
        contacts: response.data.data
      })
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      })
    })
  }

  _onEditContact = (id) => {
    console.log('editing', id)
    this.props.navigation.navigate('Details',
    {
      contactId :id
    })
  }

  _onDeletePressed = (id) => {
    Alert.alert(
      'Warning!',
      'Are you sure you want to delete this contact?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this._onDeleteContact(id)}
      ],
      {cancelable: false},
    )
  }

  _onDeleteContact = (id) => {
    console.log('deleting', id)
    this.setState({ loading: true })
    RestRequester.deleteContact(id)
    .then(response => {
      console.log('response from delete api', response)
      this.setState({
        loading:false,
        contacts: response.data.data
      })
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      })
      Alert.alert('Delete Error', 'Error deleting dontacts, please try again')
    })
  }

  _onAddContact = () => {
    this.props.navigation.navigate('Details')
  }

  _renderItem = ({ item }) => {
    return (
      <ContactRow item={item} onEdit={this._onEditContact} onDelete={this._onDeletePressed} />
    )
  }

  _setNavigationParams = () => {
    let title = 'Contacts'
    let headerRight =
      <View style={styles.headerRight}>
        <Icon
          name='person-add'
          color='white'
          onPress={
            () => {
              this._onAddContact()
            }
          }/>
      </View>
    let headerStyle = {
      backgroundColor: '#2d6882'
    }
    let headerTintColor = 'white'

    this.props.navigation.setParams({
      title,
      headerRight,
      headerStyle,
      headerTintColor
    })
  }
  
  render () {
    return (
      <View>
        <FlatList
          ListEmptyComponent={
            <Text>{ 'no contact available '}</Text>
          }
          data={this.state.contacts}
          renderItem={this._renderItem}
          keyExtractor={item => item.id}
          refreshControl={
            <RefreshControl
              refreshing = { this.state.loading }
              onRefresh = { () => {
                this._getContacts()
              }}
            />
          }
          />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerRight: {
    margin: 8
  }
})
export default ContactList
