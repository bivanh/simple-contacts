import React, {useState} from 'react'
import {
  StyleSheet,
  View
} from 'react-native'
import { Button, Divider, Icon, ListItem } from 'react-native-elements'

const _getInitials = (string) => {
  var names = string.split(' '),
      initials = names[0].substring(0, 1).toUpperCase();
  
  if (names.length > 1) {
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
}

const ContactRow = ({item, onEdit, onDelete}) => {
  const [isExpand, setIsExpand] = useState(false)
  console.log('initial isExpand', isExpand)
  const fullName = [item.firstName, item.lastName].join(' ')
  const initials = _getInitials(fullName)
  return (
    <View>
      <ListItem
        key={item.id}
        leftAvatar={{
          source: item.photo && { uri: item.photo },
          title: initials
        }}
        title={fullName}
        subtitle={ `Age: ${item.age} years`}
        onPress={ () => {
          setIsExpand(!isExpand)
          console.log('expand', isExpand)
        }}
      />
      {
        isExpand && 
        <View style={ styles.actionContainer }>
          <Button
            icon={
              <Icon
                name="edit"
                size={20}
                color="#808080"
              />
            }
            type='clear'
            onPress={ () => onEdit(item.id) }
            containerStyle={ styles.actionButton }
          />
          <Button
            icon={
              <Icon
                name="delete"
                size={20}
                color="#808080"
              />
            }
            type='clear'
            onPress={ () => onDelete(item.id) }
            containerStyle={ styles.actionButton }
          />
        </View>

        }
      <Divider style={{ backgroundColor: '#ededed' }} />
    </View>
  )
}

const styles = StyleSheet.create({
  actionButton: {
    padding: 8
  },
  actionContainer: {
    backgroundColor: '#fafafa',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  divider: {
    backgroundColor: '#ededed',
    margin: 8
  }
})
export default ContactRow
