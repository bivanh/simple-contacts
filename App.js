/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import ContactList from './screens/ContactList'
import ContactDetails from './screens/ContactDetails'
import { createStackNavigator, createAppContainer } from "react-navigation"

class App extends Component {
  render() {
    return (
      // <ContactList />
      <AppContainer />
    );
  }
}
const AppNavigator = createStackNavigator(
  {
    Contact: ContactList,
    Details: ContactDetails
  },
  {
    initialRouteName: 'Contact'
  }
)

const AppContainer = createAppContainer(AppNavigator)

export default (App)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
