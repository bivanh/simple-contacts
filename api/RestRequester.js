import axios from 'axios'
const BASE_URL = 'https://simple-contact-crud.herokuapp.com'

var RestRequester = {
  getContacts: async function () {
    return axios.get(
      BASE_URL + '/contact',
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
  },
  getContactById: async function (id) {
    return axios.get(
      BASE_URL + `/contact/${id}`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
  },
  deleteContact: async function (id) {
    return axios.delete(
      BASE_URL + `/contact/${id}`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
  },
  updateContact: async function (id, firstName, lastName, age, photo) {
    return axios.put(
      BASE_URL + `/contact/${id}`,
      {
        "firstName": firstName,
        "lastName": lastName,
        "age": parseInt(age),
        "photo": photo
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
  },
  saveContact: async function (firstName, lastName, age, photo) {
    return axios.post(
      BASE_URL + '/contact',
      {
        "firstName": firstName,
        "lastName": lastName,
        "age": parseInt(age),
        "photo": photo
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
  }
}

export default RestRequester